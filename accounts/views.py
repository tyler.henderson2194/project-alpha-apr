from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from accounts.forms import AccountLoginForm, CreateAccountForm
from django.contrib.auth import authenticate, login, logout


def account_login(request):
    if request.method == "POST":
        form = AccountLoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect("home")
    else:
        form = AccountLoginForm()
    context = {"form": form}
    return render(request, "accounts/account_login.html", context)


def account_logout(request):
    logout(request)
    return redirect("login")


def create_new_account(request):
    if request.method == "POST":
        form = CreateAccountForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]
            if password == password_confirmation:
                user = User.objects.create_user(username, password=password)
                login(request, user)
                return redirect("list_projects")
            else:
                form.add_error("password", "The passwords do not match")

    else:
        form = CreateAccountForm()

    context = {"form": form}

    return render(request, "registration/signup.html", context)
