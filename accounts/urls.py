from django.urls import path
from accounts.views import account_login, create_new_account
from django.contrib.auth import logout
from django.shortcuts import redirect


def account_logout(request):
    logout(request)
    return redirect("login")


urlpatterns = [
    path("login/", account_login, name="login"),
    path("logout/", account_logout, name="logout"),
    path("signup/", create_new_account, name="signup"),
]
